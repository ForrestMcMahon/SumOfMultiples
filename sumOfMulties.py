import timeit # Not used in actual calculation

def prob1t1(limit,factorA,factorB):
    '''Multiplies the average multiple by the number of multiples. Super quick!'''
    answer = 0
    limit = limit-1
    limitA = limit
    while limitA%factorA != 0:
        limitA -= 1
    answer += (((limitA+factorA)/2.0)*limitA/factorA)
    # Adds the sum of all multiples of factorB
    limitB = limit
    while limitB%factorB != 0:
        limitB -= 1
    answer += (((limitB+factorB)/2.0)*limitB/factorB)
    # Adds the sum of all multiples of factorA
    limitM = limit
    factorM = factorA*factorB
    while limitM%(factorM) != 0:
        limitM -= 1
    answer -= (((limitM+(factorM))/2.0)*limitM/(factorM))
    # Removes the overlap from the multiples of factorA and factorB.
    # There might be an even better version of this function that has no
    # overlap. If it exists, it should be faster, however, I couldn't find it.
    return answer

def prob1t2(limit,factorA,factorB):
    '''Generates and sums an array containing mutliples of factors A and B.'''
    answer = 0
    for x in range(0,limit,factorA):
        answer += x
    for x in range(0,limit,factorB):
        answer += x
    for x in range(0,limit,(factorA*factorB)):
        answer -= x
    return answer*1.0

def prob1t3(limit,factorA,factorB):
    '''Iterates from 1 to limit adding all multiples of factorA and factorB.'''
    answer = 0
    i = 0
    while i < limit:
        if i%factorA == 0 or i%factorB == 0:
            answer = i + answer
        i += 1
    return answer

# Tested on my desktop. Your results may vary.
print timeit.timeit('prob1t1(1000,5,3)',number=100000,setup="from __main__ import prob1t1")
# Took ~0.25
print timeit.timeit('prob1t2(1000,5,3)',number=100000,setup="from __main__ import prob1t2")
# Took ~1.75
print timeit.timeit('prob1t3(1000,5,3)',number=100000,setup="from __main__ import prob1t3")
# Took ~13.50

# My math heavy version of the standard problem has an estimated 54x speed
# improvement over the standard answer. It also had a 7x speed improvement over
# a "pythonic" answer.
